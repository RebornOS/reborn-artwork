# Contributing artwork

### What to contribute
* Reborn-themed wallpapers
* Any other wallpaper that you created and want to share
* Reborn-related artwork like 3D models or modifyed Reborn logos that can help other people to create Reborn-themed artwork themselves.

Make sure that your artwork submission does not contain any copyrighted material.

### Submit artwork
You can submit artwork by choosing one of these options:
* Upload your artwork to the [#artwork-and-screenshots](https://discord.gg/WBg9TPd) channel on our [discord server](https://discord.gg/E7pByRt) together with the text "artwork submission", use a file hoster if your file exceeds the maximum allowed file size (normally 8MiB).
* If you don't want to use Discord you can send your artwork to <damian101@posteo.de>. Either as an attachment (50MB maximum) or by uploading the file(s) to a file hoster and sending the link.

Make sure you send your file in the highest quality available. Lossless formats like PNG are always preferred over lossy formats like JPEG.

### Update existing artwork
You can of course request the update or removal of existing artwork.
Just choose on of the options shown in the "**Submit artwork**" section above.

### Additional notes
Images in lossless formats will be reencoded to PNG. The Huffman coding layer of JPEG files will be optimized to increase compression without quality loss. All irrelevant metadata will be deleted.
This can result in a reduction in file size.